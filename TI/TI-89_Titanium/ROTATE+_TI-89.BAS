()
Prgm
ClrDraw
0->c
0->si
int(rand()*7+2)->sides
approx(180/pi)->rads
(rand()*7+3)/rads->ph
approx(sin(ph))->sinph
approx(cos(ph))->cosph
rand()*360/rads->ph1
approx(sin(ph1))->sinph1
approx(cos(ph1))->cosph1
(90-180/sides)/rads->th
approx(sin(th))->sth
approx(pi-ph-th)->ph2
approx(sin(ph2))->sph2
2->rn
400/rads->circ
approx(2*pi/sides)->si
approx(cos(si))->b
approx(sin(si))->a
rand()*3+1->scx
rand()*3+1->scy
approx(pi*int(rand()*2)-1)->ph3
approx(cos(ph3))->cosph3
approx(sin(ph3))->sinph3
0->centrex
0->centrey
approx(centrex+scx*rn*cos(ph1))->oldxc
approx(centrey+scy*rn*sin(ph1))->oldyc
Lbl lbl0
sinph1->a1
cosph3*cosph1-sinph3*sinph1->b1
ph1->w
While w<=ph1+circ
w+si->w
a1->a2
a1*b+a*b1->a1
b1*b-a2*a->b1
centrex+scx*rn*b1->xcoord
centrey+scy*rn*a1->ycoord
Line oldxc,oldyc,xcoord,ycoord
xcoord->oldxc
ycoord->oldyc
EndWhile
ph1+ph->ph1
sinph*cosph1+cosph*sinph1->sinph1n
cosph*cosph1-sinph*sinph1->cosph1
sinph1n->sinph1
If sth!=0 Then 
sph2*(rn/sth)->rn
Else
0->rn
EndIf
If abs(xcoord)<90 Then
Goto lbl0
EndIf
EndPrgm
