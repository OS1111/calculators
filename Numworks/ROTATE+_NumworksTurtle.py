from math import pi,trunc,sin,cos
from random import random
from turtle import *

params = {"A": 0, 
"B": 0, 
"SCX": 0, 
"SCY": 0, 
"STH": 0, 
"RN": 0, 
"PH": 0, 
"PH1": 0, 
"SPH2": 0, 
"SI": 0, 
"CIRC": 0, 
"SINPH": 0, 
"COSPH": 0, 
"SINPH1": 0, 
"COSPH1": 0, 
"SINPH3": 0, 
"COSPH3": 0, 
"oldXcoord": 0, 
"oldYcoord": 0, 
"xcoord": 0, 
"ycoord": 0, 
"centreX": 0, 
"centreY": 0, 
"exitCd": 0}

def drawOneRot(params):
  colVal = 1
  A1 = params["SINPH1"]
  B1 = (params["COSPH3"] * params["COSPH1"]) - (params["SINPH3"] * params["SINPH1"])
  W = params["PH1"]
  while (W <= (params["PH1"] + params["CIRC"])):
    W = (W + params["SI"])
    A2 = A1
    A1 = (A1 * params["B"]) + (params["A"] * B1)
    B1 = (B1 * params["B"]) - (A2 * params["A"])
    params["xcoord"] = params["centreX"] + (params["SCX"] * params["RN"] * B1)
    params["ycoord"] = params["centreY"] + (params["SCY"] * params["RN"] * A1)
    try:
      goto(params["xcoord"],params["ycoord"])
    except Exception as e:
      print(e)
      params["exitCd"] = 1
      break
    params["oldXcoord"] = params["xcoord"]
    params["oldYcoord"] = params["ycoord"]
  params["PH1"] = params["PH1"] + params["PH"]
  SINPH1NEW = (params["SINPH"] * params["COSPH1"]) + (params["COSPH"] * params["SINPH1"])
  params["COSPH1"] = (params["COSPH"] * params["COSPH1"]) - (params["SINPH"] * params["SINPH1"])
  params["SINPH1"] = SINPH1NEW
  if (params["STH"] != 0):
    params["RN"] = params["SPH2"] * (params["RN"] / params["STH"])
  else:
    params["RN"] = 0

def startR(params):
  C = 0
  params["SIDES"] = trunc((random() * 7) + 3)
  RADS = 180 / pi
  params["PH"] = ((random() * 7) + 3) / RADS
  params["SINPH"] = sin(params["PH"])
  params["COSPH"] = cos(params["PH"])
  params["PH1"] = (random() * 360) / RADS
  params["SINPH1"] = sin(params["PH1"])
  params["COSPH1"] = cos(params["PH1"])
  params["TH"] = (90 - (180 / params["SIDES"])) / RADS
  params["STH"] = sin(params["TH"])
  params["PH2"] = pi - params["PH"] - params["TH"]
  params["SPH2"] = sin(params["PH2"])
  params["RN"] = 2
  params["CIRC"] = 400 / RADS
  params["SI"] = (2 * pi) / params["SIDES"]
  params["B"] = cos(params["SI"])
  params["A"] = sin(params["SI"])
  params["SCX"] = (random() * 3) + 1
  params["SCY"] = (random() * 3) + 1
  params["PH3"] = pi * trunc((random() * 2) - 1)
  params["COSPH3"] = cos(params["PH3"])
  params["SINPH3"] = sin(params["PH3"])
  params["oldXcoord"] = params["centreX"] + (params["SCX"] * params["RN"] * params["COSPH1"])
  params["oldYcoord"] = params["centreY"] + (params["SCY"] * params["RN"] * params["SINPH1"])
  reset()
  speed(10)
  hideturtle()
  penup()
  goto(params["oldXcoord"],params["oldYcoord"])
  pendown()

startR(params)
while (abs(params["xcoord"]) < 200):
  if (params["exitCd"] != 0):
    break
  drawOneRot(params)
print(abs(params["xcoord"]))
