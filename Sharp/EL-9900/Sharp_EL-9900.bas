ClrDraw
GridOff
AxesOff
Rad
{12,1}→dim(mat A)
randInt(0,7)+3→mat A(1,1)
180÷π→mat A(2,1)
((random x7)+3)÷mat A(2,1)→mat A(3,1)
sin(mat A(3,1))→J
cos(mat A(3,1))→K
(random x360)÷mat A(2,1)→G
sin(G)→H
cos(G)→I
(90-(180÷mat A(1,1)))÷mat A(2,1)→mat A(9,1)
sin (mat A(9,1))→mat A(4,1)
π-mat A(3,1)-mat A(9,1)→mat A(5,1)
sin(mat A(5,1))→mat A(10,1)
2→P
400÷mat A(2,1)→mat A(8,1)
(2xπ)÷mat A(1,1)→L
cos (L)→M
sin (L)→N
(random x3)+1→O
(random x3)+1→mat A(6,1)
πx(randInt(0,2)-1)→mat A(7,1)
cos (mat A(7,1))→U
sin (mat A(7,1))→V
0→Q
0→R
Q+(OxPxI)→S
R+(mat A(6,1)xPxH)→T
Label A
H→A
(UxI)-(VxH)→B
G→W
G+mat A(8,1)→Z
While W≤Z
W+L→W
A→C
(AxM)+(NxB)→A
(BxM)-(CxN)→B
Q+(OxPxB)→X
R+(mat A(6,1)xPxA)→Y
Line(S,T,X,Y)
X→S
Y→T
WEnd
G+mat A(3,1)→G
(JxI)+(KxH)→D
(KxI)-(JxH)→I
D→H
If mat A(4,1)≠0
Then
mat A(10,1)x(P÷mat A(4,1))→P
Else
0→P
EndIf
If abs(X)<700
Then
Goto A
EndIf
Pause
